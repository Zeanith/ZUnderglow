//PRETTY SERIOUS TODO: For some reason address sanitizer cannot be used on this project

#include "raylib.h" //must include raylib headers before windows.h
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
//raygui styles
#include "styles/style_cyber.h"              // raygui style: cyber
#include "raymath.h"
#define CloseWindow CloseWindowDummy //must declare these defines before including windows.h
#define Rectangle RectangleDummy
#define COBJMACROS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <windows.h>
#include <Audioclient.h>
#include <Mmdeviceapi.h>
#pragma comment(lib,"ole32")
#pragma comment(lib,"Ws2_32")
#pragma comment(lib,"user32")
#pragma comment(lib,"gdi32")
#pragma comment(lib,"shell32")
#pragma comment(lib,"ucrt")
#pragma comment(lib,"msvcrt")
#pragma comment(lib,"winmm")
#undef CloseWindow //must undef after windows.h inclusion
#undef DrawText
#undef LoadImage
#undef Rectangle

//Martins Assert
#include <intrin.h>
#define Assert(cond) do { if (!(cond)) __debugbreak(); } while (0)
#define AssertHR(hr) Assert(SUCCEEDED(hr))

typedef UINT8 u8; 
typedef UINT32 u32; 
typedef INT32 s32; 
typedef float f32;
#define global static

typedef Color colour; //Yes, this is the correct spelling
typedef Vector2 v2;
#pragma pack(push,1)
typedef struct
{
  u8 r;
  u8 g;
  u8 b;
}rgbled;
#pragma pack(pop)

typedef struct wasapicontext
{
  WAVEFORMATEX *sysbufferformat;
  IAudioClient *audioclient;
  IAudioCaptureClient *captureclient;
}wasapicontext;
typedef struct
{
  u32 loopbackbuffersize;
  u32 totalbins;
  f32 smoothingfactor;
  u32 previewmax;
  f32 *channelsamples;
  f32 *reffrequencies;
  f32 *finalmagnitudes;
}dspstuff;

//dll load function macros/types
#define CUSTOM_RENDERER(name) void name(colour *colourarray,u32 numleds)
typedef CUSTOM_RENDERER(custom_renderer_function);
CUSTOM_RENDERER(CustomRenderStub)
{
  for(int i = 0; i<3; ++i)
  {
    colourarray[i] = WHITE; //If we see some first 3 white leds, stub function is being called
  }
}
global custom_renderer_function *CustomRender;

//NOTE: This straight up dosent work if running from a debugger
void ZLoadCustomRenderer(custom_renderer_function *customrenderer, FILETIME *lastwritetime,HMODULE *temprenderhandle)
{
  WIN32_FIND_DATAA currentfiledetails;
  HANDLE newfiledetailshandle = FindFirstFileA("custom_renderer.dll", &currentfiledetails);
  FindClose(newfiledetailshandle);
  if(newfiledetailshandle != INVALID_HANDLE_VALUE)
  {
    FILETIME currentwritetime = currentfiledetails.ftLastWriteTime;
    if(CompareFileTime(&currentwritetime,lastwritetime)!=0) // 0 indicates there is a difference in filetimes, therefore we must reload
    {
      *lastwritetime = currentwritetime;
      FreeLibrary(*temprenderhandle);
      CopyFileA("custom_renderer.dll","custom_renderer_temp.dll",FALSE);
      *temprenderhandle = LoadLibraryA("custom_renderer_temp.dll");
      if(*temprenderhandle)
      {
        CustomRender = (custom_renderer_function *)GetProcAddress(*temprenderhandle,"CustomRender");
        if(!CustomRender)
        {
          OutputDebugStringA("Failed to reload CustomRender\n");
          CustomRender = CustomRenderStub;
        }
      }
    }
  }
}

//Probably lift this to shared library
void ZFillColourArray(colour *colourarray, u32 numleds, colour c)
{
  for(int i = 0; i < numleds; ++i)
  {
    colourarray[i] = c;
  }
}

void DoFastDFTOnChannel(f32 *channel, int sizeofchannel, f32 *frequencies,
    int totalbins,f32 *output, f32 smoothingfactor,
    f32 smoothingfactordifference)
{
  f32 realmagnitudesum;
  f32 imagmagnitudesum;
  f32 cosphaseincrement;
  f32 sinphaseincrement;
  f32 newsin,newcos;
  for(int binfreqindex = 0; binfreqindex<totalbins; ++binfreqindex)
  {
    sinphaseincrement = sinf(((2*PI)/48000.0f)*frequencies[binfreqindex]);
    cosphaseincrement = cosf(((2*PI)/48000.0f)*frequencies[binfreqindex]);
    realmagnitudesum = 0;
    imagmagnitudesum = 0;
    f32 cos = 1;
    f32 sin = 0;
    for(int i = 0; i<sizeofchannel;++i)
    {
      realmagnitudesum += (channel[i] * cos);
      imagmagnitudesum += (channel[i] * sin);
      newsin = (sin*cosphaseincrement) + (cos*sinphaseincrement);
      newcos = (cos*cosphaseincrement) - (sin*sinphaseincrement);
      cos = newcos;
      sin = newsin;
    }
    f32 NewFinalMagnitude = (realmagnitudesum*realmagnitudesum) + (imagmagnitudesum*imagmagnitudesum);
    output[binfreqindex] = (smoothingfactor*output[binfreqindex]) + (smoothingfactordifference*NewFinalMagnitude);
  }
}

void CaptureAudioSamples(wasapicontext *wasapistuff, f32 *channelsamples)
{
  u32 numberofchannels = wasapistuff->sysbufferformat->nChannels;
  BYTE *audiobytedata;
  DWORD flags;
  u32 framesavailable;
  u32 framecounter = 0;
  //Assume 480 frames
  AssertHR(IAudioCaptureClient_GetBuffer(wasapistuff->captureclient,&audiobytedata,&framesavailable,&flags,0,0));
  f32 *audiofloatdata= (f32 *)audiobytedata;
  for(int i = 0; i<framesavailable; ++i)
  {
    channelsamples[i]  = audiofloatdata[0]; //Left channel
    audiofloatdata+=numberofchannels; //skip over rest of channels
  }
  AssertHR(IAudioCaptureClient_ReleaseBuffer(wasapistuff->captureclient,framesavailable));
}

u32 CalculateMeanBassAmp(dspstuff *leftchanneldsp)
{
  f32 mean = 0;
  f32 smoothingfactordifference = 1.0f-leftchanneldsp->smoothingfactor;
  DoFastDFTOnChannel(leftchanneldsp->channelsamples,leftchanneldsp->loopbackbuffersize,leftchanneldsp->reffrequencies,leftchanneldsp->totalbins,leftchanneldsp->finalmagnitudes,leftchanneldsp->smoothingfactor, smoothingfactordifference);
  for(int i = 0; i<leftchanneldsp->totalbins; ++i)
  {
    mean += leftchanneldsp->finalmagnitudes[i];
  }
  mean /= leftchanneldsp->totalbins;
  mean/=10;
  int bassamp = (int)mean;
  static int OldBassAmp = 0;
  bassamp = (int)(((f32)OldBassAmp*leftchanneldsp->smoothingfactor)+((f32)bassamp*smoothingfactordifference)); //Adding secondary smoothing to Bass
  if(bassamp>leftchanneldsp->previewmax)
  {
    bassamp = leftchanneldsp->previewmax;
  } else if(bassamp<0)
  {
    bassamp = 0;
  }
  OldBassAmp = bassamp;
  return bassamp;
}

void debugprint1val(char *str, u32 val)
{
  char buf[30];
  wsprintf(buf,str,val);
  OutputDebugStringA(buf);
}

void PresetControls(colour *colourarray, u32 numleds, wasapicontext *wasapistuff, dspstuff *leftchanneldsp)
{
  static s32 presetmode = 2;
  static s32 togglemasterswitch = 0;
  u32 leftmargin = 20;
  GuiGroupBox((Rectangle){20,90,240,104},"Master Switch");
  GuiToggleSlider((Rectangle){29,100,220,85},"OFF;ON",&togglemasterswitch);
  if(togglemasterswitch)
  {
    GuiGroupBox((Rectangle){leftmargin,211,240,180},"Light Modes");
    GuiToggleGroup((Rectangle){leftmargin+9,222,220,38},"BASS\nSTATIC\nEFFECTS\nOFF",&presetmode);
    if(presetmode == 0) //Bass Reactive Mode
    {

      CaptureAudioSamples(wasapistuff,leftchanneldsp->channelsamples); //NOTE: This fails silently if AudioClient is stopped, must be started
      u32 meanbasslmplitudeleft = CalculateMeanBassAmp(leftchanneldsp);
      //debugprint1val("deepbass: %i\n", meanbasslmplitudeleft);
      f32 bassratio = (f32)meanbasslmplitudeleft/(f32)leftchanneldsp->previewmax;
      u32 underglowstriplength = numleds/2;
      u32 basslimit = (u32)(bassratio*underglowstriplength);
      static Color basscolour = {0,0,255,255};
      for(int i = 0; i<underglowstriplength; ++i)
      {
        if(i<basslimit)
        {
          colourarray[i] = basscolour;
          colourarray[underglowstriplength+i] = basscolour;
        } else
        {
          colourarray[i] = (Color){0,0,0,255};
          colourarray[underglowstriplength+i] = (Color){0,0,0,255};
        }
      }
      static f32 bassmaxfloat = 195.0f;
      GuiGroupBox((Rectangle){leftmargin,411,400,150},"Bass Controls");
      GuiSlider((Rectangle){leftmargin+110,421,247,30},"Bass max",TextFormat("%i",(int)leftchanneldsp->previewmax),&bassmaxfloat, 0, 3000);
      GuiSlider((Rectangle){leftmargin+110,461,247,30},"Bass smoothing",TextFormat("%1.2f",leftchanneldsp->smoothingfactor),&leftchanneldsp->smoothingfactor, 0, 1);
      leftchanneldsp->previewmax = (u32)bassmaxfloat;
      GuiGroupBox((Rectangle){280,90,306,301},"Bass Colour");
      GuiColorPicker((Rectangle){290,100,262,281},"Bass Colour",&basscolour);

    } else if(presetmode == 1) //Static Mode
    {
      static colour staticcolour = {0,0,255,255};
      GuiGroupBox((Rectangle){280,90,306,301},"Static Colour");
      GuiColorPicker((Rectangle){290,100,262,281},"Static Colour",&staticcolour);
      ZFillColourArray(colourarray,numleds,staticcolour);

    } else if(presetmode == 2) //Effects Mode
    {
      static colour staticcolour = {0,255,0,255};
      static int effectslistindex = 0;
      static int effectslistactive = 0;
      GuiGroupBox((Rectangle){280,90,306,301},"Effects Primary Colour");
      GuiColorPicker((Rectangle){290,100,262,281},"Effects Primary Colour",&staticcolour);
      GuiGroupBox((Rectangle){leftmargin,410,240,240},"Effects");
      GuiListView((Rectangle){ leftmargin+9, 420, 220, 220 }, "Hadouken;Rainbow;Phaser;Scatter;Cycle;Comet;Kamehameha;Strobe;Glitter;Bounce", &effectslistindex, &effectslistactive);
      GuiLabel((Rectangle){270,500,350,40}, "NOTE: None of these effects are actually implemented kekw");
      ZFillColourArray(colourarray,numleds,staticcolour);
    } else if(presetmode == 3) //off
    {
      ZFillColourArray(colourarray,numleds,BLACK);
    }
  } else //master switch of off, blast zero'd packets
  {
    ZFillColourArray(colourarray,numleds,BLACK);
  }
}

typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;
SOCKET ZSetupWinsock(void)
{
  WSADATA wsadata;
  if(WSAStartup(MAKEWORD(2,2), &wsadata))
  {
    OutputDebugStringA("WSAStartup Failed!\n");
    WSACleanup();
    return 1;
  }
  SOCKET udpsocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(udpsocket == INVALID_SOCKET)
  {
    OutputDebugStringA("socket call failed!\n");
    WSACleanup();
    return 1;
  }
  //Host details
  sockaddr_in bindaddress;
  bindaddress.sin_family = AF_INET;
  bindaddress.sin_port = htons(3278);
  bindaddress.sin_addr.s_addr = INADDR_ANY;
  if(bind(udpsocket, (sockaddr *)&bindaddress, sizeof(bindaddress)))
  {
    OutputDebugStringA("bind call failed!\n");
    WSACleanup();
    return 1;
  }
  return udpsocket;
}

void ZCopyColourArrayToLEDArray(colour *colours, rgbled *leds, u32 numleds)
{
  u32 striplength = numleds/2;
  for(int i = 0; i < striplength; ++i)
  {
    leds[striplength-i-1] = (rgbled){colours[i].r,colours[i].g,colours[i].b}; //reverse direction of first strip to mirror second strip orientation
    leds[i+striplength] = (rgbled){colours[i].r,colours[i].g,colours[i].b};
  }
}

void reversefirstarray(colour *colours, u32 numleds)
{
  u32 striplength = numleds/2;
  for(int i = 0; i<striplength/2; ++i)
  {
    colour temp = colours[i];
    colours[i+striplength-1] = colours[i];
    colours[i+striplength-1] = temp;
  }
}

static const IID CLSID_MMDeviceEnumerator = {0xBCDE0395, 0xE52F, 0x467C, {0x8E, 0x3D, 0xC4, 0x57, 0x92, 0x91, 0x69, 0x2E}}; // BCDE0395-E52F-467C-8E3D-C4579291692E
static const IID IID_IMMDeviceEnumerator = {0xA95664D2, 0x9614, 0x4F35, {0xA7, 0x46, 0xDE, 0x8D, 0xB6, 0x36, 0x17, 0xE6}}; // A95664D2-9614-4F35-A746-DE8DB63617E6
static const IID IID_IAudioClient = {0x1CB9AD4C, 0xDBFA, 0x4c32, {0xB1, 0x78, 0xC2, 0xF5, 0x68, 0xA7, 0x03, 0xB2}}; //1CB9AD4C-DBFA-4c32-B178-C2F568A703B2
static const IID IID_IAudioCaptureClient = {0xC8ADBD64, 0xE71E, 0x48a0, {0xA4, 0xDE, 0x18, 0x5C, 0x39, 0x5C, 0xD3, 0x17}}; //C8ADBD64-E71E-48a0-A4DE-185C395CD317

int SetupWASAPIStuff(wasapicontext *wasapistuff)
{
  //Sets up lookback buffer on default audio device
  u32 bufferframecount;
  IMMDeviceEnumerator *deviceenumerator = 0;
  IMMDevice *device = 0;
  IAudioClient *audioclient = 0;
  AssertHR(CoInitializeEx(NULL, COINIT_APARTMENTTHREADED));
  AssertHR(CoCreateInstance(&CLSID_MMDeviceEnumerator, 0, CLSCTX_ALL,&IID_IMMDeviceEnumerator, (void **)&deviceenumerator));
  AssertHR(IMMDeviceEnumerator_GetDefaultAudioEndpoint(deviceenumerator,eRender,eConsole,&device));
  AssertHR(IMMDevice_Activate(device,&IID_IAudioClient,CLSCTX_ALL,0,(void **)&audioclient));
  AssertHR(IAudioClient_GetMixFormat(audioclient,&wasapistuff->sysbufferformat));
  AssertHR(IAudioClient_Initialize(audioclient,AUDCLNT_SHAREMODE_SHARED,AUDCLNT_STREAMFLAGS_LOOPBACK,0,0,wasapistuff->sysbufferformat,NULL));
  AssertHR(IAudioClient_GetService(audioclient,&IID_IAudioCaptureClient,(void **)&wasapistuff->captureclient));
  AssertHR(IAudioClient_Start(audioclient)); //Start recording
  return 0;
}

void SetupReferenceFrequencyTable(f32 *referencefrequencies,int binsperoctave, f32 basefrequency,int octaves)
{
  //Generate Reference frequencies of equal temperment
  u32 indexonpiano = 37;
  u32 frequencyindex = 0;
  u32 totalbins = binsperoctave * octaves;
  u32 iterations = indexonpiano+totalbins;
  for(int i = indexonpiano; i<iterations;++i)
  {
    referencefrequencies[frequencyindex++] = basefrequency*(pow((pow(2.0f,(1.0f/12.0f))),(i-49)));
  }
}

int CALLBACK WinMain(HINSTANCE inst, HINSTANCE previnst, LPSTR cmdline, int cmdshow)
{
  u32 windowwidth = 1280;
  u32 windowheight = 760;

  //Will probably be global variables/struct
  u32 numleds = 240; //hardcoded single strip assumption for now
  u32 underglowstriplength = numleds/2; //hardcoded single strip assumption for now
  u32 sizeofcolourarray = sizeof(colour)*numleds;
  u32 sizeofsendleds = sizeof(rgbled)*numleds;
  u32 sizeofsendpacket = (sizeof(rgbled)*numleds)+3; //3 byte header then rgbled array
  colour *colourarray = malloc(sizeofcolourarray); //modify colours with raygui color
                                                   //elements then convert to rgbleds
                                                   //at end of frame
  rgbled *sendleds = malloc(sizeofsendleds);
  char *sendpacket = malloc(sizeofsendpacket);
  //Bruh, all these mallocs - where the areans at?

  //setup UDP socket
  SOCKET socket = ZSetupWinsock();
  if(socket == 1)
  {
    OutputDebugStringA("ZSetupSocket Failed!\n");
    return 1;
  }
  sockaddr_in sendaddress;
  sendaddress.sin_family = AF_INET;
  sendaddress.sin_port = htons(7777);
  //sendaddress.sin_addr.s_addr = inet_addr("192.168.4.1");
  sendaddress.sin_addr.s_addr = inet_addr("127.0.0.1");

  //Live Reload DLL stuff
  CustomRender = CustomRenderStub;
  CustomRender(colourarray,numleds);
  CopyFileA("custom_renderer.dll","custom_renderer_temp.dll",FALSE);
  WIN32_FIND_DATAA filedetails;
  HANDLE findfilehandle = FindFirstFileA("custom_renderer.dll", &filedetails);
  FindClose(findfilehandle);
  FILETIME dlllastwritetime = filedetails.ftLastWriteTime;
  HMODULE customrendererhandle = LoadLibraryA("custom_renderer_temp.dll");
  if(customrendererhandle)
  {
    CustomRender = (custom_renderer_function *)GetProcAddress(customrendererhandle,"CustomRender");
    if(!CustomRender)
    {
      CustomRender = CustomRenderStub;
    }
  }

  //WASAPI stuff
  wasapicontext wasapistuff;

  InitWindow(windowwidth, windowheight, "ZUnderglow v0 - Handmade Reinvention Jam 2023");
  Image carimg = LoadImage("../resources/car.png");
  Image carimgtrans = ImageCopy(carimg);
  ImageResize(&carimgtrans,302,150);
  ImageRotateCCW(&carimgtrans);
  Texture2D car = LoadTextureFromImage(carimgtrans);
  SetTargetFPS(144);
  GuiLoadStyleCyber();
  {
    Image icon = LoadImage("../resources/icon.png");
    SetWindowIcon(icon);
  }

  //Gui Variables
  s32 modeselect = 0; 
  bool dropdownedit = 0;

  //Music viz variables
  u32 binsperoctave = 12;
  u32 numoctaves = 2;
  //NOTE: 0.92 smoothing and 195 previewmax seem to be reasonable defaults for songs I wish to wise, in reality optimal values for this varies widely per song.
  f32 basssmoothingfactor = 0.87;
  u32 basspreviewmax = 195;
  u32 totalbins = binsperoctave*numoctaves;
  f32 *refbassfreqs = (f32 *)malloc(totalbins*sizeof(f32));
  f32 *finalbassmagnitudesleft = (f32 *)malloc(totalbins*sizeof(f32));

  dspstuff leftchanneldsp;
  leftchanneldsp.loopbackbuffersize = 480;
  leftchanneldsp.totalbins = binsperoctave*numoctaves;;
  leftchanneldsp.smoothingfactor = basssmoothingfactor;
  leftchanneldsp.previewmax = basspreviewmax;
  leftchanneldsp.reffrequencies = refbassfreqs;
  leftchanneldsp.finalmagnitudes = finalbassmagnitudesleft;

  SetupWASAPIStuff(&wasapistuff);
  leftchanneldsp.loopbackbuffersize = wasapistuff.sysbufferformat->nSamplesPerSec/100;
  f32 *leftchannelsamples = (f32 *)malloc(sizeof(f32)*leftchanneldsp.loopbackbuffersize);
  leftchanneldsp.channelsamples = leftchannelsamples;
  while(!WindowShouldClose())
  {
    BeginDrawing();
    ClearBackground(BLACK);
    if(modeselect == 0) // Setup Mode
    {
      GuiGroupBox((Rectangle){650,80,473,570},"LED strip orientation");
      //setup gui variables
      static char ipaddressfield[20] = "127.0.0.1"; //should be set to 192.168.4.1 for esp8266ws2812i2s firmware
      static bool textboxedit;
      static bool valueboxedit;
      static int sendport = 7777; //default port for esp8266ws2812i2s
      memset(colourarray,0,sizeofcolourarray);
      {
        u32 compheight = 30;
        u32 compwidth = 125;
        u32 compx = 25;
        u32 compy = 80;
        GuiGroupBox((Rectangle){compx-10,compy-10,(compwidth+12)*2,(compheight+12)*2},"Network Settings");
        GuiSetStyle(TEXTBOX, TEXT_ALIGNMENT, TEXT_ALIGN_CENTER);
        if (GuiTextBox((Rectangle){ compx, compy, compwidth, compheight }, ipaddressfield, 20, textboxedit)) textboxedit = !textboxedit;
        if(GuiButton((Rectangle){compx+compwidth+5,compy,compwidth,compheight}, "set ipaddress"))
        {
          sendaddress.sin_addr.s_addr = inet_addr(ipaddressfield);
        }
        compy+=(compheight+5);
        if (GuiValueBox((Rectangle){ compx, compy, compwidth, compheight }, NULL, &sendport, 0, 65535, valueboxedit)) valueboxedit = !valueboxedit;
        if(GuiButton((Rectangle){compx+compwidth+5,compy,compwidth,compheight}, "set port number"))
        {
          sendaddress.sin_port = htons(sendport);
        }

        //Strip direction toggles (just for show, these dont actually do anything)
        {
          static bool leftsidedirection = true; //up
          static bool rightsidedirection = true; //up
          static bool topsidedirection = true; //right
          static bool bottomsidedirection = true; //right
          if(leftsidedirection)
          {
            if(GuiButton((Rectangle){700,280,100,200}, "#121#"))
            {
              leftsidedirection = false;
            }
          } else
          {
            if(GuiButton((Rectangle){700,280,100,200}, "#120#"))
            {
              leftsidedirection = true;
            }
          }
          if(rightsidedirection)
          {
            if(GuiButton((Rectangle){975,280,100,200}, "#121#"))
            {
              rightsidedirection = false;
            }
          } else
          {
            if(GuiButton((Rectangle){975,280,100,200}, "#120#"))
            {
              rightsidedirection = true;
            }
          }
          if(topsidedirection)
          {
            if(GuiButton((Rectangle){785,100,200,100}, "#119#"))
            {
              topsidedirection = false;
            }
          } else
          {
            if(GuiButton((Rectangle){785,100,200,100}, "#118#"))
            {
              topsidedirection = true;
            }
          }
          if(bottomsidedirection)
          {
            if(GuiButton((Rectangle){785,525,200,100}, "#119#"))
            {
              bottomsidedirection = false;
            }
          } else
          {
            if(GuiButton((Rectangle){785,525,200,100}, "#118#"))
            {
              bottomsidedirection = true;
            }
          }
        }
      }
    } else if(modeselect == 1) //Preset Mode
    {
      PresetControls(colourarray,numleds,&wasapistuff,&leftchanneldsp);
    } else if(modeselect == 2) //Custom Mode
    {
      ZLoadCustomRenderer(CustomRender,&dlllastwritetime,&customrendererhandle);
      CustomRender(colourarray,numleds);
    }
    if(GuiDropdownBox((Rectangle){30,20,200,40},"Setup;Preset;Custom",&modeselect,dropdownedit))
    {
      dropdownedit = !dropdownedit;
    }
    //Render led preview
    u32 ledpreviewbottommargin = GetRenderHeight()-10;
    u32 ledpreviewpadding= 2;
    u32 rectsize = 4;
    v2 ledrectpos = {640,ledpreviewbottommargin};
    v2 ledrectsize = {200,rectsize};
    v2 ledrectpos2 = {ledrectpos.x+ledrectsize.x+100,ledpreviewbottommargin};

    Color fillcolour = {0,0,0};
    for(int i = 0; i<underglowstriplength; ++i)
    {
      fillcolour = colourarray[i];
      DrawRectangleV(ledrectpos,ledrectsize,fillcolour);
      DrawRectangleV(ledrectpos2,ledrectsize,fillcolour);
      ledrectpos.y-=(ledrectsize.y+ledpreviewpadding);
      ledrectpos2.y-=(ledrectsize.y+ledpreviewpadding);
    }
    ZCopyColourArrayToLEDArray(colourarray,sendleds,numleds);
    memcpy(sendpacket+3,sendleds,sizeofsendleds);
    sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
    DrawFPS(1190,15);
    DrawTexture(car,815,210,RED);
    EndDrawing();
  }
  memset(sendleds,0,sizeofsendleds);
  memcpy(sendpacket+3,sendleds,sizeofsendleds);
  sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
  sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
  sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
  sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
  sendto(socket,sendpacket,sizeofsendpacket,0, (sockaddr *)&sendaddress,sizeof(sendaddress)); //sendleds
  UnloadImage(carimg);
  UnloadImage(carimgtrans);
  UnloadTexture(car);
  CloseWindow();
  return 0;
}
