**CAUTION:** This project was done in haste for the Handmade Network Wheel Reinvention Jam 2023. 

# ZUnderglow

Native windows app to setup, display and write custom effects for vehicle underglow. Project made for [Handmade Network Wheel Reinvention Jam 2023](https://handmade.network/jam/2023)

## Instructions
NOTE: This app is pretty useless unless you have a wifi led receiver setup, I have a description of my setup [here](https://handmade.network/p/436/zunderglow/)

### Compiling
Compile with msvc by running build.bat. Assumes vcvarsall setup environment. 

**PRO TIP:** I use this [script](https://gist.github.com/mmozeiko/7f3162ec2988e81e56d5c4e22cde9977) from mmozeiko to setup my environment.

#### Known Issues
* Sometimes bass mode shows 100% with no audio playing. Restarting the app has fixed this in my experience. Otherwise, if audio is playing and you see 100% levels consistently, try playing with "bass preview max" slider as the bass level could be clipping. Different values work best for different songs/system audio levels.
