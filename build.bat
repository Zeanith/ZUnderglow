@echo off

if not exist build (mkdir build)
pushd build
REM remove /O2 if you wish to step through this code
cl /FC ..\custom_renderer.c /O2 /nologo /Zi /Zo /GS- /LD /link /fixed /incremental:no /opt:icf /opt:ref /subsystem:windows

cl ..\main.c /FC /O2 /nologo /FeZUnderglow.exe /Zi /Zo /GS- /link /fixed /incremental:no /opt:icf /opt:ref /subsystem:windows ..\raylib.lib
popd
